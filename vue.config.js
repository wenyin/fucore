module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  pages: {
    'index': {
      entry: './src/pages/index/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: "Fucore Welcome Page",
      // chunks: ['chunk-common', 'index', 'index']
    },
    'institute': {
      entry: './src/pages/institute/main.js',
      template: 'public/index.html',
      filename: 'institute.html',
      title: "Fucore Institute Page",
      // chunks: ['chunk-vendors', 'chunk-common', 'institute']
    },
    'machine': {
      entry: './src/pages/machine/main.js',
      template: 'public/index.html',
      filename: 'machine.html',
      title: "Fucore Machine Page",
      // chunks: ['chunk-vendors', 'chunk-common', 'machine']
    },
    'dailyexpt': {
      entry: './src/pages/dailyexpt/main.js',
      template: 'public/index.html',
      filename: 'dailyexpt.html',
      title: "Fucore Daily Experiment Page",
      // chunks: ['chunk-vendors', 'chunk-common', 'dailyexpt']
    },
    'shot': {
      entry: './src/pages/shot/main.js',
      template: 'public/index.html',
      filename: 'shot.html',
      title: "Fucore Shot Analysis Page for 2020102001",
      // chunks: ['chunk-vendors', 'chunk-common', 'shot']
    },
    // 'about': {
    //   entry: './src/pages/about/main.js',
    //   template: 'public/index.html',
    //   filename: 'about.html',
    //   title: "Fucore About Page",
    //   // chunks: ['chunk-vendors', 'chunk-common', 'about']
    // }
  }
}