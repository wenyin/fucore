# Structure

- src/, 
  - pages/, contains multiple subfolders representing various pages that constitute the MPA (multiple-pages application) app, electron. `*.vue` files and javascript entry files `main.js` combine to build a single window.   
  - components/, 
  - assets/, include svg images.
  - plugins/, legacy folder, not yet decided what to do.
- public/,
- node_modules/, 
- [`vue.config.js`](https://cli.vuejs.org/config/#vue-config-js), an config file that will be automatically loaded by `@vue/cli-service`. You can also use the Vue field in `package.json`, but do note in that case you will be limited to JSON-compatible values only. In short, the file export an object containing options.
- `package.json`, determines the dependencies.
- 