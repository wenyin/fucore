# Fucore Coding Construction Record
## 安装 Install
首先安装 [vue-cli](https://cli.vuejs.org/)
```bash
npm install -g @vue/cli
# OR
yarn global add @vue/cli
```

然后你可以新建一个项目了，
```bash
vue create my-project
# 记得不要选择 Vue3 版本，Vuetify 对 Vue3 的支持大概在 2021 年出来。
# 另外不要选 router
```


electron 在安装的时候会非常慢，如果有需要的话（人在国内），就看这个[帖子](https://segmentfault.com/a/1190000020932174?utm_source=tag-newest)，这里我们特别地临时性配置下面这两行就够用了。

```
npm config set registry https://registry.npm.taobao.org/
npm config set ELECTRON_MIRROR http://npm.taobao.org/mirrors/electron/
```


做好准备了就可以安装 electron-builder 了，否则在国内可能会面临多达一个小时的安装时间。
```
vue add electron-builder
```

现在你可以试着运行了，可能会尝试运行几次都失败，因为有一个 extension 需要联网，可能会碰到类似下面的 warning，[问题](https://github.com/nklayman/vue-cli-plugin-electron-builder/issues/776)不大，不用管它

```
INFO  Launching Electron...
(node:2531) ExtensionLoadWarning: Warnings loading extension at /home/ununtu/.config/fucore/extensions/nhdogjmejiglipccpnnnanhbledajbpd: Unrecognized manifest key 'browser_action'. Unrecognized manifest key 'update_url'. Permission 'contextMenus' is unknown or URL pattern is malformed. Cannot load extension with file or directory name _metadata. Filenames starting with "_" are reserved for use by the system. 
```

现在如果你尝试运行 `npm run electron:serve` 的话可能还是会失败，比如说有一些 `module not found ` 比如说 `vue-cli-service`,`electron-devtools-installer`，`electron`，缺什么装什么就可以了。

```
(c)npm install -g @vue/cli-service
(c)npm install --save electron-devtools-installer
(c)npm install --save electron
```





现在安装 UI 库 [vuetify](https://vuetifyjs.com/en/) 和可视化库 d3.

```
vue add vuetify
(c)npm install d3
```
[How to implement D3 for Vue.js](https://stackoverflow.com/questions/41067022/how-to-implement-d3-for-vue-js), [Vue D3 V5 Example](https://gywgithub.github.io/vue-d3-examples/#/), [D3 + Vue Exapmle](https://www.redblobgames.com/x/1842-vue-d3/)

### Packages

[Vue-plotly](https://github.com/David-Desmaisons/vue-plotly)

[Plotly.js](https://plotly.com/javascript/) 在 d3.js 和 stack.gl 之上建立起来。

[OpenLayers](https://openlayers.org/) 作为地图使用。

[Vue 官网 Map 集锦](https://vuejsexamples.com/tag/maps/)

[Vue 地图库视频教程](https://www.syncfusion.com/vue-ui-components/vue-maps)

[Plotly: Maps Tutorial](https://plotly.com/javascript/maps/)

[Plotly: Mapbox Map Layers in JavaScript](https://plotly.com/javascript/mapbox-layers/)

[Plotly: Scatter Plots on Maps in JavaScript](https://plotly.com/javascript/scatter-plots-on-maps/)

## SPA to MPA 从单页面应用到多页面应用

[Multiple Pages for Vue CLI Plugin Electron Builder](https://nklayman.github.io/vue-cli-plugin-electron-builder/guide/recipes.html#multiple-pages)

[multiple pages in Vue.js CLI](https://stackoverflow.com/questions/51692018/multiple-pages-in-vue-js-cli)

[Vue CLI "pages" Configuration](https://cli.vuejs.org/config/#pages)

[Vue CLI3基础学习之pages构建多页应用](https://www.jb51.net/article/162352.htm) 详细的中文介绍多入口和多模板下如何编写 MPA，不过不适合 Electron


## Communicate with Other Languages

### Python

#### [PYTHON AND VUE](https://pusher.com/tutorials/game-python-vue)

#### [nbconvert](https://github.com/jupyter/nbconvert)

The nbconvert tool, jupyter nbconvert, converts notebooks to various other formats via Jinja templates. The nbconvert tool allows you to convert an .ipynb notebook file into various static formats including:

- HTML
- LaTeX
- PDF
- Reveal JS
- Markdown (md)
- ReStructured Text (rst)
- executable script

[How to embed Jupyter HTML output in a web page](https://stackoverflow.com/questions/41520034/embed-jupyter-html-output-in-a-web-page)

#### [Jupyter Notebooks in Web Pages](https://thedatafrog.com/en/articles/jupyter-notebooks-web-pages/)
In this post, you'll learn how to set up a web page to communicate scientific results with style!

When you're done with this exercise, you will know how to:

integrate perfectly a jupyter notebook in a web page,
apply an elegant overall style with Bootstrap,
highlight code with Pygments,
render math equations with MathJax.


#### [Talking to Python from JavaScript (and Back Again!)](https://healeycodes.com/javascript/python/beginners/webdev/2019/04/11/talking-between-languages.html)

We’ll look at two ways that these two languages can communicate. AJAX requests via the new Fetch API, and piping between local processes.

Serializing data means taking a value, object, or data structure, and translating it into a format that can be stored or transmitted. Most importantly, it needs to be put back together at the other end. Let’s take look at JavaScript Object Notation (JSON).

#### [How to Get Python and JavaScript to Communicate Using JSON](https://www.makeuseof.com/tag/python-javascript-communicate-json/)

#### [Run Python Script using PythonShell from Node.js](https://www.geeksforgeeks.org/run-python-script-using-pythonshell-from-node-js/)

#### [How to communicate between Python and NodeJs](https://www.sohamkamani.com/blog/2015/08/21/python-nodejs-comm/)

In this tutorial, we will be using the child_process standard library in nodeJs to spawn a python process which will compute the sum of all elements in an array, using numpy, and return back the result to our node program.

### C++

#### [How to Improve Node.js Productivity With C++ Addons: Step by Step Guide](https://apiko.com/blog/node-js-performance-c-addons/)

As a result, Node is C++ friendly and allows developers to run C++ code in Node.js applications. This is done with the help of Node addons.

Node.js Addons are dynamically-linked shared objects, written in C++. They can be loaded into Node.js using the require() function, and used just as if they were an ordinary Node.js module. They are used primarily to provide an interface between JavaScript running in Node.js and C/C++ libraries.

#### [How can I use a C++ library from node.js?](https://stackoverflow.com/questions/9629677/how-can-i-use-a-c-library-from-node-js#:~:text=%20three%20general%20ways%20of%20integrating%20C%2B%2B%20code,C%20code%20as%20a%20native%20Node.js...%20More%20)

- [node-ffi](https://github.com/node-ffi/node-ffi)
- [SWIG](http://www.swig.org/) is a software development tool that connects programs written in C and C++ with a variety of high-level programming languages. SWIG is used with different types of target languages including common scripting languages such as Javascript, Perl, PHP, Python, Tcl and Ruby.

## Visualization
### [Making Python plots in web browser or iframe](https://www.scivision.dev/python-html-plotting-iframe-share/)
- [mpl3d](https://mpld3.github.io/quickstart.html)
- 

### [matplotnode](https://www.npmjs.com/package/matplotnode)
C++ bindings for Node.js exposing a subset of matplotlib's functionality through the CPython API. Inspired by matplotlib-cpp by lava. Useful for scientific plotting.

### [NodePlotLib](https://github.com/ngfelixl/nodeplotlib)
Library to create top-notch plots directly within NodeJS on top of plotly.js without any front-end preparations. Inspired by matplotlib.

## Update
想要检测自动更新，安装更新依赖 `yarn add electron-updater`，同时修改 `vue.config.js` 文件添加 `publish` 配置，一定要有这个，可以生成`latest.yml` 文件用于版本检测对比
```js
module.exports = {
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        publish: [
           {
            provider: "generic",
            channel: "latest",
            url: "http://xxxxxx/dist_electron/"
           }
        ]
      }
    }
  }
}
```
作者：krystal__H
链接：https://www.jianshu.com/p/69ce3a2d59c9
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。