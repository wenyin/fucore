# fucore

## Project setup
```
/path/to/fucore > npm install
```

Theoretically, the above code is the only code you would need to install all packages that are needed for fucore project. But I am not sure whether you would need to run 

```bash
/path/to/fucore > npm install -g @vue/cli
# OR
/path/to/fucore > yarn global add @vue/cli
```

to let Vue-cli work.



If you are in China, maybe  you would need to run the following code to accelerate the download.

```bash
/path/to/fucore > npm config set registry https://registry.npm.taobao.org/
/path/to/fucore > npm config set ELECTRON_MIRROR http://npm.taobao.org/mirrors/electron/
```



### Compiles and hot-reloads for development

```bash
/path/to/fucore > npm run electron:serve
```

### Compiles and minifies for production
```bash
/path/to/fucore > npm run electron:build
```

