'use strict'

import { app, protocol, BrowserWindow } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer'
const isDevelopment = process.env.NODE_ENV !== 'production'

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let window_vec = []

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])

function createWindow(devPath, prodPath) {
  // Adjust the window size by what the page is.
  let suggestedSize
  if (devPath == "index")         {suggestedSize = {width: 1000, height: 800}}
  else if (devPath=="institute")  {suggestedSize = {width: 800, height: 600}}
  else if (devPath=="machine")    {suggestedSize = {width: 800, height: 600}}
  else if (devPath=="dailyexpt")  {suggestedSize = {width: 800, height: 600}}
  else if (devPath=="shot")       {suggestedSize = {width: 800, height: 600}}
  // Create the browser window.
  let window = new BrowserWindow(suggestedSize)

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    window.loadURL(process.env.WEBPACK_DEV_SERVER_URL + devPath)
    if (!process.env.IS_TEST) window.webContents.openDevTools()
  } else {
    // Load the index.html when not in development
    window.loadURL(`app://./${prodPath}`)
  }
  window.on('closed', () => { window = null })
  return window
}

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    window_vec.push( createWindow('index', 'index.html') )
  }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installExtension(VUEJS_DEVTOOLS)
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  if (!process.env.WEBPACK_DEV_SERVER_URL) {
    createProtocol('app')
  }
  window_vec.push( createWindow('index', 'index.html') )
  window_vec.push( createWindow('institute', 'institute.html') )
  window_vec.push( createWindow('machine', 'machine.html') )
  window_vec.push( createWindow('dailyexpt', 'dailyexpt.html') )
  window_vec.push( createWindow('shot', 'shot.html') )
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})


let { ipcMain } = require('electron')

ipcMain.on('create_institute_window', (event, {institute}) => {
  let win = createWindow('institute', 'institute.html')
  window_vec.push( win )
  win.webContents.send('new_institute_window_arg', {institute})
})

ipcMain.on('create_machine_window', (event, {machine}) => {
  let win = createWindow('machine', 'machine.html')
  window_vec.push( win )
  win.webContents.send('new_machine_window_arg', {machine})
})

ipcMain.on('create_dailyexpt_window', (event, {year, month, day}) => {
  let win = createWindow('dailyexpt', 'dailyexpt.html')
  window_vec.push( win )
  win.webContents.send('new_dailyexpt_window_arg', {year, month, day})
})

ipcMain.on('create_shot_window', (event, {shotNo}) => {
  let win = createWindow('shot', 'shot.html')
  window_vec.push( win )
  win.webContents.send('new_shot_window_arg', {shotNo})
})
// TODO:
// home page -> institute page 
// If I receive data from home page saying that the user wanna reach his or her own institute,
// I turn on that page and load the corresponding data about its machines.

// institute page -> machine page 
// you know which machine the user is clicking on, 
// and then open that page and load machine calender data and machine parameter and structure.BrowserWindow

// machine page -> dailyexpt page 
// click on each date on the calendar and then you know all shots of that day.BrowserWindow
// then you come to the dailyexpt page 

// dailyexpt page -> shot page 


// each page should have a button the return to the home page.